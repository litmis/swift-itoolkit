//
//  TabViewController.swift
//  Swift_XMLSERVICES
//
//  Created by inmer on 6/22/16.
//  Copyright © 2016 ibmer. All rights reserved.
//

import Foundation
import UIKit

class TabViewController: UIViewController {
    
    // PGM call variables
    @IBOutlet var name1: UILabel!
    @IBOutlet var title1: UILabel!
    @IBOutlet var url1: UILabel!
    @IBOutlet var name2: UILabel!
    @IBOutlet var title2: UILabel!
    @IBOutlet var url2: UILabel!
    @IBOutlet var name3: UILabel!
    @IBOutlet var title3: UILabel!
    @IBOutlet var url3: UILabel!
    
    // DB2 call variables
    @IBOutlet var disk1: UIProgressView!
    @IBOutlet var disk2: UIProgressView!
    @IBOutlet var disk3: UIProgressView!
    
    // PASE call variables
    @IBOutlet var paseOutput: UITextView!
    
    @IBOutlet var activityInd: UIActivityIndicatorView!
    
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    let toolKit = iToolKit(url: "http://lp0364d.rch.stglabs.ibm.com/cgi-bin/xmlcgi.pgm", db2: "*LOCAL", uid: "DB2", pwd: "NICE2DB2", ipc: "*debugproc", ctl: "*here")
    
    override func viewDidLoad() {

        super.viewDidLoad()
        
        // Hide all view before viewing the correct one based on the button pressed
        self.view.viewWithTag(1)?.hidden = true
        self.view.viewWithTag(2)?.hidden = true
        self.view.viewWithTag(3)?.hidden = true
        // Activate indicator right after button pressed
        activityInd.startAnimating()
        
        if appDelegate.callSelection == 1 {
            //self.view.viewWithTag(1)?.hidden = false
            pgmCall()
        }
        else if appDelegate.callSelection == 2 {
            //self.view.viewWithTag(2)?.hidden = false
            db2Call()
            
        }
        else if appDelegate.callSelection == 3 {
            //self.view.viewWithTag(3)?.hidden = false
            paseCall()
        }
        
        // SImple CMD Example
        //CMD()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func pgmCall() {
        
        // Using the toolkit with raw XML
        let pgm =  "?db2=*LOCAL"
            + "&uid=DB2"
            + "&pwd=NICE2DB2"
            + "&ipc=*debugproc"
            + "&ctl=*here"
            + "&xmlin=0"
            + "<?xml version='1.0'?>"
            + "<myscript>"
            + "<cmd error='fast'>CHGLIBL LIBL(XMLSERVICE) CURLIB(XMLSERVICE)</cmd>"
            + " <pgm name='EZ1SRV' func='EMPBYTAG' error='fast'>"
            + "  <parm><data type='128a' varying='2'>php</data></parm>"
            + "  <parm><data type='10i0'>3</data></parm>"
            + "  <parm><data type='10i0' enddo='mycount'></data></parm>"
            + "  <parm>"
            + "   <ds dim='50' dou='mycount'>"
            + "    <data type='10i0'></data>"
            + "    <data type='5p0'></data>"
            + "    <data type='128a' varying='2'></data>"
            + "    <data type='512a' varying='4'></data>"
            + "    <data type='512a' varying='4'></data>"
            + "    <data type='10a'></data>"
            + "    <data type='8a'></data>"
            + "    <data type='124000a' varying='4'></data>"
            + "    <data type='124000a' varying='4'></data>"
            + "   </ds>"
            + "  </parm>"
            + " </pgm>"
            + "</myscript>"
            + "&xmlout=0"
        
        toolKit.add(pgm)
        appDelegate.req = toolKit.req()
        toolKit.call{(resXML) -> Void in
            self.activityInd.stopAnimating()
            self.parsePGMCall(resXML)
        }
        
    }
    
    func db2Call() {
        
        //toolKit.addDB2("select UNIT_STORAGE_CAPACITY from QSYS2.SYSDISKSTAT")
        //toolKit.addDB2("select PERCENT_USED from QSYS2.SYSDISKSTAT")
        toolKit.addDB2("select * from QSYS2.SYSDISKSTAT")
        appDelegate.req = toolKit.req()
        toolKit.call{(resXML) -> Void in
            self.activityInd.stopAnimating()
            self.parseDB2Call(resXML)
        }
        
    }
    
    func paseCall() {

        toolKit.addPASE("/QOpenSys/usr/bin/system wrkactjob | grep -i fr")
        // Set the request label value before it's cleared in the call method
        appDelegate.req = toolKit.req()
        // Waits for the asynchronous call to return before proceeding
        toolKit.call{(resXML) -> Void in
            // Following code is executed after the call is returned
            // Stop indicator when data is returned before parsing
            self.activityInd.stopAnimating()
            self.parsePaseCall(resXML)
        }
        
    }
    
    func CMD() {
        
        toolKit.addCMD("RTVJOBA USRLIBL(?) SYSLIBL(?)")
        toolKit.call{(resXML) -> Void in
            self.parseCMDCall(resXML)
        }
        
    }
    
    // Parsing is done using AEXML
    func parsePGMCall(XMLData: NSData) {
        
        do {
            let xmlDoc = try AEXMLDocument(xmlData: XMLData)
            print(xmlDoc.xmlString)
            appDelegate.res = xmlDoc.xmlString
            var attributes: [String] = []
            
            appDelegate.res = xmlDoc.stringValue
            for i in 0 ..< xmlDoc.root["pgm"].children[3].children.count {
                for child in xmlDoc.root["pgm"].children[3].children[i]["data"].all! {
                    if let type = child.attributes["type"] {
                        if type == "128a" {
                            attributes.append(child.stringValue)
                            //print(child.stringValue)
                        }
                        else if type == "512a"{
                            attributes.append(child.stringValue)
                            //print(child.stringValue)
                        }
                    }
                }
            }// for
            
            // Needs to be updated in the main thread, not background
            dispatch_async(dispatch_get_main_queue()) {
                self.name1.text = attributes[0]
                self.title1.text = attributes[1]
                self.url1.text = attributes[2]
                
                self.name2.text = attributes[3]
                self.title2.text = attributes[4]
                self.url2.text = attributes[5]
                
                self.name3.text = attributes[6]
                self.title3.text = attributes[7]
                self.url3.text = attributes[8]
                
                // Show the results after parsing the response data
                self.view.viewWithTag(1)?.hidden = false
            }
            
        }
        catch {
            print("\(error)")
        }
    }
    
    func parseDB2Call(XMLData: NSData) {
        
        do {
            let xmlDoc = try AEXMLDocument(xmlData: XMLData)
            print("\n\n")
            print(xmlDoc.xmlString)
            appDelegate.res = xmlDoc.xmlString
            var diskUsage: [String] = []
            
            for i in 0 ..< (xmlDoc.root["sql"]["fetch"].children.count - 1)  {
                for child in xmlDoc.root["sql"]["fetch"].children[i]["data"].all! {
                    if let desc = child.attributes["desc"] {
                        if desc == "PERCENT_USED" {
                            diskUsage.append(child.stringValue)
                            //print(child.stringValue)
                        }
                    }
                }
            }
            dispatch_async(dispatch_get_main_queue()) {
                self.disk1.setProgress(((Float(diskUsage[0])!)/100), animated: true)
                self.disk2.setProgress(((Float(diskUsage[1])!)/100), animated: true)
                self.disk3.setProgress(((Float(diskUsage[2])!)/100), animated: true)
                self.view.viewWithTag(2)?.hidden = false
            }
        }
        catch {
            print("\(error)")
        }
    }
    
    func parsePaseCall(XMLData: NSData) {
        
        do {
            let xmlDoc = try AEXMLDocument(xmlData: XMLData)
            //print(xmlDoc.xmlString)
            appDelegate.res = xmlDoc.xmlString
            var paseString = ""
            
            for row in xmlDoc.root["sh"].children {
                let strArr = row.value!.componentsSeparatedByString(" ")
                var strArrNoSpaces: [String] = []
                for word in strArr {
                    if word != "" {
                        strArrNoSpaces.append(word)
                    }
                }
                paseString += "Job: " + strArrNoSpaces[0] + "\n"
                            + "User: " + strArrNoSpaces[1] + "\n"
                            + "Number: " + strArrNoSpaces[2] + "\n"
                            + "CPU: " + strArrNoSpaces[7] + "\n\n"
            }
            dispatch_async(dispatch_get_main_queue()) {
                self.paseOutput.text = paseString
                self.view.viewWithTag(3)?.hidden = false
            }
        }
        catch {
            print("\(error)")
        }
        
    }
    
    func parseCMDCall(XMLData: NSData) {
        
        do {
            let xmlDoc = try AEXMLDocument(xmlData: XMLData)
            print(xmlDoc.xmlString)
        }
        catch {
            print("\(error)")
        }
    }

}