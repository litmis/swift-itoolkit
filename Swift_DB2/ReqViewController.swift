//
//  ReqViewController.swift
//  Swift_XMLSERVICES
//
//  Created by inmer on 6/22/16.
//  Copyright © 2016 ibmer. All rights reserved.
//

import Foundation
import UIKit

class ReqViewController: UIViewController {
    
    @IBOutlet var req: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        req.text = appDelegate.req
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}