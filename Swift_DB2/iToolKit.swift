//
//  ToolKit.swift
//  Swift_XMLSERVICES
//
//  Created by inmer on 6/27/16.
//  Copyright © 2016 ibmer. All rights reserved.
//
//  IBMi Swift toolkit.
//  The toolkit calls IBMi via XMLSERVICES using HTTP REST
//  AEXML used as input and output parser
//
//  Examlple:
//    // Create a toolkit
//    let toolKit = iToolKit(url: "http://lp0364d.rch.stglabs.ibm.com/cgi-bin/xmlcgi.pgm", uid: "DB2")
//
//    // Add a query
//    toolKit.addDB2("select * from QSYS2.SYSDISKSTAT")
//
//    // Call
//    // Waits for the asynchronous call to return before proceeding
//    toolKit.call{(resXML) -> Void in
//        // parse method is called after the call is returned and resXML is avalible
//        parse(resXML)
//    }

import Foundation

class iToolKit {
    
    var url: String
    var db2: String
    var uid: String
    var pwd: String
    var ipc: String
    var ctl: String
    var xmlString: String = String()
    var requests: [String] = []
    
    init(url: String, db2: String, uid: String, pwd: String, ipc: String, ctl: String) {
        self.url = url
        self.db2 = "?db2="+db2
        self.uid = "&uid="+uid
        self.pwd = "&pwd="+pwd
        self.ipc = "&ipc="+ipc
        self.ctl = "&ctl="+ctl
    }
    
    // Create toolkit wihtout optional values
    init(url: String, uid: String) {
        self.url = url
        self.db2 = "?db2=*LOCAL"
        self.uid = "&uid="+uid
        self.pwd = "&pwd=0"
        self.ipc = "&ipc=0"
        self.ctl = "&ctl=0"
    }
    
    // Simple PASE shell command formatted using AEXML parser
    func addPASE(inputCMD: String) {
        
        let Request = AEXMLDocument()
        let envelope = Request.addChild(name: "xmlservice")
        envelope.addChild(name: "sh", value: inputCMD, attributes: ["rows" : "on"])
        xmlString = db2 + uid + pwd + ipc + ctl + "&xmlin=0" + Request.xmlString + "&xmlout=32768"
        requests.append(xmlString)
    }
    
    // Simple DB2 SQL query formatted using AEXML parser
    func addDB2(inputQuery: String) {
        
        let Request = AEXMLDocument()
        let envelope = Request.addChild(name: "xmlservice")
        let query = envelope.addChild(name: "sql")
        query.addChild(name: "query", value: inputQuery)
        query .addChild(name: "fetch", attributes: ["block" : "all"])
        xmlString = db2 + uid + pwd + ipc + ctl + "&xmlin=0" + Request.xmlString + "&xmlout=512000"
        requests.append(xmlString)
    }
    
    // Simple CMD formatted using AEXML parser
    func addCMD(inputCMD: String) {
        
        let Request = AEXMLDocument()
        let envelope = Request.addChild(name: "xmlservice")
        envelope.addChild(name: "cmd", value: inputCMD, attributes: ["exec" : "rexx"])
        xmlString = db2 + uid + pwd + ipc + ctl + "&xmlin=0" + Request.xmlString + "&xmlout=32768"
        requests.append(xmlString)
    }
    
    // Complete XML
    func add(inputXML: String) {
        xmlString = inputXML
        requests.append(inputXML)
    }
    
    // REST Asynchronous Post Call
    // Returns a NSData object upon completion
    func call(completionHandler : ((resXML : NSData) -> Void)) {
        
        for i in 0 ..< requests.count {
            
            var XMLData = NSData()
            let xmlStr = requests[i]
            let request = NSMutableURLRequest(URL: NSURL(string: url)!)
            let session = NSURLSession.sharedSession()
            request.HTTPMethod = "POST"
            let post:NSString = xmlStr
            let postData:NSData = post.dataUsingEncoding(NSUTF8StringEncoding)!
            request.HTTPBody = postData
            request.addValue("text/xml", forHTTPHeaderField: "Content-Type")
            
            let task = session.dataTaskWithRequest(request, completionHandler: {data, response, error -> Void in
                XMLData = NSData()
                XMLData = data!
                self.xmlString = String()
                self.requests.removeAll()
                completionHandler(resXML : XMLData)
                //print(response)
                //print(error)
            })
            
            task.resume()
            
        }
        
    }
    
    // Used for the demo application
    // Returns the XML being called
    func req() -> String {
        return xmlString
    }
    
    
}